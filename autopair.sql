-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: autopair
-- ------------------------------------------------------
-- Server version	5.7.21-1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PO` varchar(255) DEFAULT NULL,
  `Details` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'PO-0001','SEIKEN',5,200,'2018-09-24 06:50:36'),(2,'PO-0002','Choke',10,300,'2018-09-26 06:51:05'),(3,'PO-0003','Wheel',4,1000,'2018-09-26 06:51:33'),(4,'PO-0001','Wheel',5,1000,'2018-09-26 08:04:45'),(5,'PO-0004','โช๊คอัพ',3,700,'2018-09-26 13:26:13');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PO` varchar(255) DEFAULT NULL,
  `Details` varchar(255) DEFAULT NULL,
  `employee` varchar(255) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES (1,'PO-0004','ดุมล้อ','สันติ',3,700,'2018-09-26 15:32:57'),(2,'PO-0001','ลูกปืนล้อ','แยม',5,100,'2018-09-26 15:38:44'),(3,'PO-0001','ยางกันฝุ่นเพลานอก','แยม',10,100,'2018-09-26 15:40:16'),(4,'PO-0001','Wheel','แยม',4,1200,'2018-09-26 15:42:35');
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test2`
--

DROP TABLE IF EXISTS `test2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SO` varchar(255) DEFAULT NULL,
  `Details` varchar(255) DEFAULT NULL,
  `employee` varchar(255) DEFAULT NULL,
  `order_date` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `hub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=tis620;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test2`
--

LOCK TABLES `test2` WRITE;
/*!40000 ALTER TABLE `test2` DISABLE KEYS */;
INSERT INTO `test2` VALUES (1,'SO-0001','ยางกันฝุ่นเพลาใน','แยม','26/09/2561','098-289-1556',5,150,NULL),(2,'SO-0001','ยางกันฝุ่นเพลานอก','แยม','26/09/2561','098-289-1556',10,150,NULL),(3,'SO-0001','ลูกหมากกันโคลงหน้า','แยม','26/09/2561','098-289-1556',15,200,NULL),(4,'SO-0002','ลูกหมากกันโคลงหน้า','สันติ','26/09/2561','098-289-1556',15,200,NULL),(5,'SO-0002','ยางกันกระแทกโช๊ค','สันติ','26/09/2561','098-289-1556',4,100,NULL),(6,'SO-0003','บู๊ชปีกนกตัวล่าง','รัชชต','27/09/2561','098-395-6595',6,300,NULL),(7,'SO-0003','เบ้าโช๊คอัพ/ยางเบ้าโช๊คอัพ','รัชชต','27/09/2561','098-395-6595',20,500,NULL),(8,'SO-0004','ลูกหมากกันโคลงหลัง','รัชชต','28/09/2561','098-395-6595',5,300,NULL),(9,'SO-0004','ยางกันฝุ่นแร็ค','รัชชต','28/09/2561','098-395-6595',10,150,NULL),(10,'SO-0005','ดุมล้อ/ลูกปืน','สันติ','28/09/2561','098-395-6595',4,400,NULL),(11,'SO-0005','เบ้าโช๊คอัพ/ยางเบ้าโช๊คอัพ','สันติ','28/09/2561','098-395-6595',8,1000,NULL),(12,'SO-0006','เบ้าโช๊คอัพ/ยางเบ้าโช๊คอัพ','รัชชต','28/09/2561','098-395-6595',10,1000,NULL),(13,'SO-0007','ยางกันฝุ่นเพลานอก','สันติ','28/09/2561','098-395-6595',5,200,NULL),(14,'SO-0008','ยางกันฝุ่นเพลานอก','สันติ','28/09/2561','098-395-6595',5,200,'Hub3'),(15,'SO-0009','ยางกันฝุ่นเพลานอก','สันติ','28/09/2561','098-395-6595',5,200,NULL),(16,'SO-0010','ยางกันฝุ่นเพลานอก','สันติ','28/09/2561','098-395-6595',5,200,NULL),(17,'SO-0011','ยางกันฝุ่นเพลานอก','สันติ','28/09/2561','098-395-6595',5,200,NULL);
/*!40000 ALTER TABLE `test2` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-28 23:32:59
